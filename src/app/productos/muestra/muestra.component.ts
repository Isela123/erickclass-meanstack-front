import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ProductoServicioService } from '../producto-servicio.service';
import { Producto } from '../producto.interface';

@Component({
  selector: 'app-muestra',
  templateUrl: './muestra.component.html',
  styleUrls: ['./muestra.component.css']
})
export class MuestraComponent implements OnInit {

  producto = new Array<Producto>();

  constructor(private http: HttpClient, private productoS: ProductoServicioService) { }

  ngOnInit(): void {
    this.cargarProductos();
  }

  cargarProductos(){
    this.productoS.obtenerProductos().subscribe( res => {
      console.log(res);

      this.producto=res
    })
  }

}